/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka7;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;



/**
 *
 * @author Topi
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button changeButton;
    @FXML
    private Button addTextButton;
    @FXML
    private TextField inputField;
    @FXML
    private TextField inputFileField;
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    @FXML
    private TextArea inputArea;
   
    
    
    
    
    
    
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Hello World!");
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    

    @FXML
    private void changeAction(ActionEvent event) {
        label.setText("Hello World!");
    }

    @FXML
    private void addTextAction(ActionEvent event) {
        label.setText(inputField.getText());
        inputField.setText("");
    }


    @FXML
    private void keyAddAction(KeyEvent event) {
        
        inputField.setOnKeyPressed(new EventHandler<KeyEvent>() {
    @Override
    public void handle(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER)  {
            label.setText(inputField.getText());
            inputField.setText("");
    }
    }
});
    }
    
    @FXML
    private void saveAction(ActionEvent event) throws IOException {
        String filename = inputFileField.getText();
        String text = inputArea.getText();
        
        FileWriter fw = new FileWriter(filename, true);
        BufferedWriter bw = new BufferedWriter(fw);
        try (PrintWriter pw = new PrintWriter(bw)) {
            pw.println(text);
            inputArea.setText("");
            pw.close();
        }
        }
    

    @FXML
    private void loadAction(ActionEvent event) throws FileNotFoundException {
        String filename = inputFileField.getText();
        
        File textfile = new File(filename);
        try (Scanner sc = new Scanner(textfile)) {
            while(sc.hasNextLine()) {
                inputArea.setText(inputArea.getText() + sc.nextLine() + "\n");
            
        }
    }}
}